//
// Created by javier on 23/1/2017.
//

#ifndef GNSS_SIM_TRACKING_OBS_WRITER_H
#define GNSS_SIM_TRACKING_OBS_WRITER_H

#include "GPS_L1_CA.h"
#include "datetime.h"
#include "gpstime.h"
#include <fstream>
#include <string>
#include <vector>


class tracking_obs_writer
{
public:
    ~tracking_obs_writer();
    bool write_binary_obs(double signal_timestamp_s,
        double acc_carrier_phase_cycles,
        double doppler_l1_hz,
        double prn_delay_chips,
        double tow);
    bool open_obs_file(std::string out_file);
    bool d_dump;

private:
    std::string d_dump_filename;
    std::ofstream d_dump_file;
};

#endif  //GNSS_SIM_TRACKING_OBS_WRITER_H
