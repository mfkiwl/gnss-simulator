//
// Created by javier on 3/11/15.
//

#include "gpstime.h"

gpstime::gpstime()
{
    week = -1;
    sec = 0.0;
}

double gpstime::gps2jd(const int rollover)
{
    //% GPS2JD  Converts GPS week number (since 1980.01.06) and
    //%   seconds of week to Julian date.
    //% Input:   gpsweek  - GPS week number
    //%          sow      - seconds of week since 0 hr, Sun (default=0)
    //%          rollover - number of GPS week rollovers (default=0)
    //% Output:  jd       - Julian date

    double jd_gps_start = 2444244.5;                          //julian date: (1980,1,6) beginning of GPS week numbering
    double nweek = (double)week + 1024.0 * (double)rollover;  //account for rollovers every 1024 weeks
    return (jd_gps_start + nweek * 7.0 + sec / 3600.0 / 24.0);
}
