//
// Created by javier on 3/11/15.
//

#ifndef GNSS_SIM_PSEUDORANGE_H
#define GNSS_SIM_PSEUDORANGE_H

#include "gpstime.h"
class pseudorange
{
public:
    gpstime g;
    double range;       //Range affected by clock bias and relativistic effect
    double true_range;  //true geometric unbiased range
    double rate;        //rate affected by clock drift
    double true_rate;   //true unbiased geometric range rate

    pseudorange();
};


#endif  //GNSS_SIM_PSEUDORANGE_H
