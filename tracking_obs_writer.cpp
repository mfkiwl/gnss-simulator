//
// Created by javier on 23/1/2017.
//

#include "tracking_obs_writer.h"
#include <utility>


bool tracking_obs_writer::write_binary_obs(double signal_timestamp_s,
    double acc_carrier_phase_cycles,
    double doppler_l1_hz,
    double prn_delay_chips,
    double tow)
{
    try
        {
            d_dump_file.write((char *)&signal_timestamp_s, sizeof(double));
            d_dump_file.write((char *)&acc_carrier_phase_cycles, sizeof(double));
            d_dump_file.write((char *)&doppler_l1_hz, sizeof(double));
            d_dump_file.write((char *)&prn_delay_chips, sizeof(double));
            d_dump_file.write((char *)&tow, sizeof(double));
        }
    catch (const std::ifstream::failure &e)
        {
            std::cout << "Exception writing tracking obs dump file " << e.what() << std::endl;
        }
    return true;
}

bool tracking_obs_writer::open_obs_file(std::string out_file)
{
    if (d_dump_file.is_open() == false)
        {
            try
                {
                    d_dump_filename = std::move(out_file);
                    d_dump_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
                    d_dump_file.open(d_dump_filename.c_str(), std::ios::out | std::ios::binary);
                    //std::cout << "Observables dump enabled, Log file: " << d_dump_filename.c_str()<< std::endl;
                    return true;
                }
            catch (const std::ifstream::failure &e)
                {
                    std::cout << "Problem opening Observables dump Log file: " << d_dump_filename.c_str() << std::endl;
                    return false;
                }
        }
    else
        {
            return false;
        }
}

tracking_obs_writer::~tracking_obs_writer()
{
    if (d_dump_file.is_open() == true)
        {
            d_dump_file.close();
        }
}
